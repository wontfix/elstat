import React from 'react';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './Service.css';
import Graph from './Graph.js';

const Service = ({ graph, name, status, latency, description }) => (
  <div className="service">
    <header>
      <div className="emoji">
        <FontAwesomeIcon
          icon={`${status ? 'check' : 'exclamation'}-circle`}
          style={{ color: status ? '#2ECC40' : '#FF4136' }}
        />
      </div>
      <h2 className="title">
        {name} {latency ? (
          <span className="latency">
            {latency}ms
          </span>
        ) : null}
      </h2>
    </header>
    <p className="description">
      {description}
    </p>
    {graph ? <Graph width={500} height={175} data={graph} /> : null}
  </div>
);

Service.defaultProps = {
  graph: null,
  latency: null,
};

Service.propTypes = {
  graph: PropTypes.arrayOf(
    PropTypes.arrayOf(PropTypes.number),
  ),
  name: PropTypes.string.isRequired,
  status: PropTypes.bool.isRequired,
  latency: PropTypes.number,
  description: PropTypes.string.isRequired,
};

export default Service;
