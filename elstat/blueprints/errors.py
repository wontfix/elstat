class ApiError(Exception):
    @property
    def message(self):
        return self.args[0]

    @property
    def status_code(self):
        return self.args[1]
