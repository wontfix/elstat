import datetime

from sanic import Blueprint, response

from .decorators import auth_route

bp = Blueprint(__name__)


# TODO: pages
@bp.get('/api/incidents')
async def get_incidents(request):
    manager = request.app.manager
    cur = manager.conn.cursor()

    cur.execute("""
    SELECT id, incident_type, title, content, ongoing,
        start_timestamp, end_timestamp
    FROM incidents
    ORDER BY id DESC
    """)

    rows = cur.fetchall()

    res = []

    for row in rows:
        cur = manager.conn.cursor()

        cur.execute("""
        SELECT title, content
        FROM incident_stages
        WHERE parent_id = ?
        ORDER BY timestamp ASC
        """, (row[0],))

        stage_rows = cur.fetchall()
        def stage_obj(stage_row):
            return {
                'title': stage_row[0],
                'content': stage_row[1],
            }

        stages = list(map(stage_obj, stage_rows))
        start_timestamp = datetime.datetime.fromtimestamp(row[5])
        end_timestamp = datetime.datetime.fromtimestamp(row[6])

        res.append({
            'id': str(row[0]),
            'type': row[1],
            'title': row[2],
            'content': row[3],
            'ongoing': row[4],
            'start_timestamp': start_timestamp.isoformat(),
            'end_timestamp': end_timestamp.isoformat(),
            'stages': stages
        })

    try:
        first = next(iter(res))
    except StopIteration:
        first = {'ongoing': False}

    return response.json({
        'all_good': not first['ongoing'],
        'incidents': res,
    })


@bp.put('/api/incidents')
@auth_route
async def create_incident(request):
    return response.text('im gay')
