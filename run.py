import logging
import sqlite3

import aiohttp
from sanic import Sanic, response
from sanic_cors import CORS
from sanic.exceptions import NotFound, FileNotFound

import config

from elstat.manager import ServiceManager
from elstat.blueprints import api, streaming, incidents
from elstat.blueprints.errors import ApiError

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

app = Sanic()
app.cfg = config
CORS(app, automatic_options=True)

app.blueprint(api)
app.blueprint(incidents)
app.blueprint(streaming)


@app.listener('before_server_start')
async def _app_start(refapp, loop):
    refapp.session = aiohttp.ClientSession(loop=loop)
    refapp.conn = sqlite3.connect('elstat.db')
    refapp.manager = ServiceManager(app)


@app.listener('after_server_stop')
async def _app_stop(refapp, _loop):
    refapp.manager.close()
    refapp.conn.close()


@app.exception(ApiError)
async def _handle_api_err(request, exception):
    return response.json({
        'error': True,
        'code': exception.status_code,
        'message': exception.message
    }, status=exception.status_code)


@app.exception(Exception)
async def _handle_exc(request, exception):
    log.exception('oopsie woopsie')

    status_code = 404 if isinstance(exception, (NotFound, FileNotFound)) \
        else 500

    return response.json({
        'error': True,
        'message': repr(exception)
    }, status=status_code)


if __name__ == '__main__':
    app.static('/', './priv/frontend/build')
    app.static('/', './priv/frontend/build/index.html')

    app.run(port=config.PORT, host='0.0.0.0')
