from .adapters import HttpAdapter, PingAdapter


ADAPTERS = {
    'http': HttpAdapter,
    'ping': PingAdapter,
}
