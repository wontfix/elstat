import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faCheckCircle,
  faExclamationCircle,
} from '@fortawesome/free-solid-svg-icons';

export default function register() {
  library.add(
    faCheckCircle,
    faExclamationCircle,
  );
}
