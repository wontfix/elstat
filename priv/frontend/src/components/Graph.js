import React, { Component } from 'react';

import ms from 'ms';
import { ResponsiveLine } from '@nivo/line';

import './Graph.css';

export default class Graph extends Component {
  processData() {
    const { data: unprocessedData } = this.props;

    const data = unprocessedData.map(([timestamp, latency]) => ({
      x: timestamp,
      y: latency,
    })).reverse();

    return [
      {
        id: 'latency',
        color: 'hsl(220, 100%, 75%)',
        data,
      },
    ];
  }

  isSmallScreen() {
    return window.innerWidth < 500;
  }

  render() {
    return (
      <div className="graph-container">
        <ResponsiveLine
          data={this.processData()}
          margin={{ top: 30, left: 70, bottom: 50 }}
          maxY="auto"
          curve="monotoneX"

          tooltipFormat={(d) => `${d}ms`}

          axisLeft={{
            format: (d) => `${d}ms`,
            tickCount: 3,
            legend: 'latency',
            legendPosition: 'center',
            legendOffset: -55,
            tickSize: 0,
          }}

          axisBottom={{
            format: (epoch) => {
              const interval = this.isSmallScreen() ? 7 : 5;
              const minutesAgo = Math.floor((Date.now() - epoch) / (1000 * 60));
              if (minutesAgo % interval !== 0 || minutesAgo === 0) {
                return undefined;
              }

              return ms(Date.now() - epoch);
            },
            tickSize: 0,
            legend: 'time ago',
            legendPosition: 'center',
            legendOffset: 40,
          }}

          enableDots={false}
          enableArea
        />
      </div>
    );
  }
}
