import React from 'react';

import ReactPlaceholder from 'react-placeholder';
import 'react-placeholder/lib/reactPlaceholder.css';

import './ServicePlaceholder.css';

const ServicePlaceholder = () => (
  <div className="service-placeholder">
    <div className="title">
      <ReactPlaceholder
        type="round"
        ready={false}
        style={{ width: '3rem', height: '3rem' }}
        className="emoji"
        showLoadingAnimation
      >
        {' '}
      </ReactPlaceholder>
      <ReactPlaceholder
        type="rect"
        ready={false}
        style={{ width: '100%', height: '3rem' }}
        showLoadingAnimation
      >
        {' '}
      </ReactPlaceholder>
    </div>
    <ReactPlaceholder
      type="text"
      ready={false}
      rows={1}
      style={{ marginTop: '1rem' }}
      showLoadingAnimation
    >
      {' '}
    </ReactPlaceholder>
    <ReactPlaceholder
      type="rect"
      ready={false}
      style={{ width: '100%', height: '6rem', marginTop: '1rem' }}
      showLoadingAnimation
    >
      {' '}
    </ReactPlaceholder>
  </div>
);

export default ServicePlaceholder;
