module.exports = {
  UNSUBSCRIBE: -1,
  SUBSCRIBE: 0,
  SUBSCRIBED: 1,
  UNSUBSCRIBED: 2,
  DATA: 3,
};
