export function log(...args) {
  console.log(
    '%c[elstat]%c',
    'color: purple; font-weight: bold',
    'color: inherit; font-weight: inherit',
    ...args,
  );
}
