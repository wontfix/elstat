PORT = 8069

PASSWORD = '123456'

SERVICES = {
    'elixire': {
        'description': "elixi.re's backend",
        'adapter': 'http',
        'adapter_args': {
            'url': 'https://elixi.re/api/hello'
        },
        'poll': 10
    },
    'dabian': {
        'description': 'elixi.re main server',
        'adapter': 'ping',
        'adapter_args': {
            'address': '192.168.1.1'
        },
        'poll': 15
    }
}
